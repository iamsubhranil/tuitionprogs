echo "Enter the number of elements : "
read n

echo "Enter the elements : "
for (( i=0; i<$n; i++ ))
do
    read a[$i]
done

echo -n "Enter the element to search : "
read item

low=0
up=`expr $n - 1`
while [ $low -le $up ];
do
    mid=`echo  "($low + $up) / 2" | bc`
    if [ $item -eq ${a[$mid]} ]
    then
        echo "Element found in the array!"
        exit
    elif [ $item -lt ${a[$mid]} ]
    then
        up=`expr $mid - 1`
    else
        low=`expr $mid + 1`
    fi
done

echo "Element not found in the array!"
