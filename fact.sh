echo -n "Enter the integer to generate factorial : "
read n

Fact() {
    f=1
    for (( i=1; i<=$1; i++ ))
    do
        f=`expr $f \* $i`
    done
    echo $f
}

res=`Fact $n`

echo "Factorial of $n is $res"
