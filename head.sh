#!/bin/sh

# simulate head
# head  <file>          first 10 lines
# head -n <num> <file>  first <num> lines

Head() {
    if [ -f $1 ];
    then
        if [ -r $1 ];
        then
            i=0
            while read line;
            do
                echo $line
                i=`expr $i + 1`
                if [ $i -eq $2 ];
                then
                    break
                fi
            done < $1
        else
            echo "$1: Permission denied!"
        fi
    else
        echo "$1: No such file or directory!"
    fi
}

if [ $# -eq 1 ];
then
    Head $1 10
elif [ $# -eq 3 ];
then
    if [ "$1" = "-n" ];
    then
        Head $3 $2
    else
        echo "Wrong arguments!"
    fi
else
    echo "Wrong number of arguments specified!"
fi
