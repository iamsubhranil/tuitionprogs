echo "Enter the number of elements : "
read n

echo "Enter the elements : "
for (( i=0; i<$n; i++ ))
do
    read a[$i]
done

echo -n "Enter the element to search : "
read item

for (( i=0; i<$n; i++ ))
do
    if [ ${a[$i]} -eq $item ];
    then
        echo "$item found in the array at position `expr $i + 1`"
        exit
    fi
done

echo "$item not found in the array!"
