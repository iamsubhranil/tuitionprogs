#!/bin/sh

# -m    : memory usage
# -d    : disk usage
# -h    : save to html file
# default : -m -d

if [ $# -eq 0 ]
then
    echo "Current disk usage is : "
    df -h
    echo
    echo "Current memory usage is : "
    free -h
    echo
elif [ $# -eq 1 ]
then
    if [ "$1" = "-m" ]
    then
        echo "Current memory usage is : "
        free -h
    elif [ "$1" = "-d" ]
    then
        echo "Current disk usage is : "
        df -h
    else
        echo "The argument is invalid!"
    fi
elif [ $# -eq 3 ]
then
    if [ -f $3 ]
    then
        rm $3
    fi
    if [ "$1" = "-m" -a "$2" = "-h" ]
    then
        echo -e "<body> Current memory usage is : <br><br>" > $3
        free -h > temp
        while read line
        do
            echo -e "$line" >> $3
            echo -e "\n<br>\n" >> $3
        done < temp
        echo -e "\n</body>" >> $3
    elif [ "$1" = "-d" -a "$2" = "-h" ]
    then
        echo -e "<body> Current memory usage is : <br><br>" > $3
        df -h > temp
        while read line
        do
            echo -e "$line" >> $3
            echo -e "\n<br>\n" >> $3
        done < temp
        echo -e "\n</body>" >> $3
    else
        echo "Wrong arguments!"
    fi
else
    echo "Wrong number of arguments specified!"
fi

