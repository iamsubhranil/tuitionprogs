#!/bin/sh

# 1. create XYZ in home
# 2. populate XYZ with two sub-directories
# 3. copy some shell scriptst to them from pwd

if [ -d $HOME/XYZ ]
then
    rm -rf $HOME/XYZ
fi

mkdir $HOME/XYZ
echo "Directory XYZ is created in $HOME"

echo "Enter the names of sub-directories to create : "
read n1
read n2

mkdir $HOME/XYZ/$n1 $HOME/XYZ/$n2

echo "Sub-directories created!"

s=`find $HOME/*.sh`
