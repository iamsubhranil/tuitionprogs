IsPrime() {
    x=`factor $1 | wc -w`
    echo $x
}

#IsPrime 13
#echo $?
echo -n "Enter the integer to check : "
read n

res=`IsPrime $n`

if [ $res -eq 2 ]; then
    echo "The given integer is prime!"
else
    echo "The given integer is not prime!"
fi
