IsPrime() {
    x=`factor $1 | wc -w`
    echo $x
}

echo -n "Enter the lower and upper limits : "
read low
read up

echo "Primes within the range $low and $up are : "
for (( i=$low; i<=$up; i++ ))
do
    res=`IsPrime $i`
    if [ $res -eq 2 ]; then
        echo -e "$i \c"
    fi
done
echo
