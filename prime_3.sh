IsPrime() {
    x=`factor $1 | wc -w`
    echo $x
}

echo -n "Enter the lower and upper limits : "
read low
read up

echo "Twin primes within the range $low and $up are : "
bak=2
for (( i=$low; i<=$up; i++ ))
do
    res=`IsPrime $i`
    if [ $res -eq 2 ]; then
        if [ `expr $i - $bak` -eq 2 ];
        then
            echo "$bak $i"
        fi
        bak=$i
    fi
done
echo
