echo "Enter the number of elements : "
read n

echo "Enter the elements of the array : "
for (( i=0; i<$n; i++ ))
do
    read a[$i]
done

if [ -f temp ];
then
    echo -n "temp is going to be overwritten. Confirm (Y/N)? "
    read c
    case $c in
        [Yy]*) true;;
        *) exit;;
    esac
fi

echo "${a[@]}" > temp

echo "Second largest number : "

echo `cat temp | tr ' ' '\n' | sort -u | tail -n 2 | head -n 1`
