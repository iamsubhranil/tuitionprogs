#        1     1     1             1
#   1 + --- + --- + --- + ..... + ---
#        2!    3!    4!            n!
#

echo -n "Enter the value of n : "
read n

Fact() {
    if [ $1 -le 1 ];
    then
        echo 1
    else
        x1=`expr $1 - 1`
        res=`Fact $x1`
        echo `echo "$1 * $res" | bc`
    fi
}

sum=0

for (( i=1; i<=$n; i++ ))
do
    dino=`Fact $i`
    term=`echo -e "scale=5\n1 / $dino" | bc`
    sum=`echo -e "scale=5\n$sum + $term" | bc`
done

echo "The sum is $sum"
