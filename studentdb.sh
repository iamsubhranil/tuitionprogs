#!/bin/sh

# student db with following fields 
# ROLL  NAME    STREAM  YEAR    DOB

echo "Enter the db filename : "
read fname

echo "Enter the number of records to store : "
read n

for (( i=1; i<=$n; i++ ))
do
    echo "Roll : "
    read roll
    echo "Name : "
    read name
    echo "Stream : "
    read strm
    echo "Year : "
    read year
    echo "DOB (DD/MM/YY) : "
    read dob
    
    grep -w "$roll" $fname > temp
    if [ -s temp ]
    then
        echo "Student with roll no $roll already exists!"
        continue
    else
        echo -e "$roll\t$name\t$strm\t$year\t$dob" >> $fname
    fi
done
