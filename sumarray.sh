a=()
echo "Enter the number of elements : "
read n

for (( i=0; i<$n; i++ ))
do
    read a[$i]
done

echo
sum=0
echo "The elements of array are : "
for (( i=0; i<$n; i++ ))
do
    sum=`expr $sum + ${a[$i]}`
    echo ${a[$i]}
done

echo "The sum  of elements is $sum"
