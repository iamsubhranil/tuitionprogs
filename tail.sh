#!/bin/sh

# simulate tail
# tail  <file>          first 10 lines
# tail -n <num> <file>  first <num> lines

Tail() {
    if [ -f $1 ];
    then
        if [ -r $1 ];
        then
            count=`cat $1 | wc -l`
            start=`echo "$count - $2 + 1" | bc`
            i=1
            while read line;
            do
                if [ $i -ge $start ];
                then
                    echo $line
                fi
                i=`expr $i + 1`
            done < $1
        else
            echo "$1: Permission denied!"
        fi
    else
        echo "$1: No such file or directory!"
    fi
}

if [ $# -eq 1 ];
then
    Tail $1 10
elif [ $# -eq 3 ];
then
    if [ "$1" = "-n" ];
    then
        Tail $3 $2
    else
        echo "Wrong arguments!"
    fi
else
    echo "Wrong number of arguments specified!"
fi
