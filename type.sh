#!/bin/sh

# simulation of DOS type

if [ $# -eq 0 ];
then
    echo "A file name must be specified!"
elif [ $# -eq 1 ];
then
    if [ -f $1 ];
    then
        if [ -r $1 ];
        then
            while read line 
            do
                echo -e "$line"
            done < $1
        else
            echo "$1: Permission denied!"
        fi
    else
        echo "$1: No such file or directory!"
    fi
else
    echo "Wrong number of argument specified!"
fi
