#!/bin/sh

if [ $# -eq 0 ];
then
    echo "Atleast one argument must be specified!"
    exit
fi

totallines=0
totalwords=0
totalchars=0
# Considering only 1 argument
Wc() {
    if [ -f $1 ];
    then
        if [ -r $1 ];
        then
            file=$1
            lines=0
            words=0
            chars=0
            while read line;
            do
                lines=`expr $lines + 1`
                c=`expr length "$line"`
                chars=`expr $chars + $c`
                w=`echo $line | head -n 1 | tr ' ' '\n' | grep -c -e ""`
                words=`expr $words + $w`
            done < $file
            echo -e "$lines\t$words\t$chars\t$file"
            totallines=`expr $totallines + $lines`
            totalwords=`expr $totalwords + $words`
            totalchars=`expr $totalchars + $chars`
        else
            echo "$1: Permission denied!"
        fi
    else
        echo "$1: No such file!"
    fi
}

for i in "$*"
do
    Wc $i
done

if [ $# -gt 1 ];
then
    echo -e "$totallines\t$totalwords\t$totalchars\ttotal"
fi
